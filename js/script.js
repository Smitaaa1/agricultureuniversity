$(".gallery-slider").slick({
  centerMode: false,
  arrows: false,

  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,

        slidesToShow: 1,
      },
    },
  ],
});

$(".message-slider").slick({
  centerMode: false,
  arrows: false,

  slidesToShow: 3,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToScroll: 3,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToScroll: 3,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToScroll: 3,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToScroll: 2,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToScroll: 2,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,

        slidesToShow: 1,
      },
    },
  ],
});

$(".leader-slider").slick({
  centerMode: false,
  arrows: true,

  slidesToShow: 2,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,

        slidesToShow: 1,
      },
    },
  ],
});
$(".hero-slider").slick({
  // centerMode: true,
  arrows: true,
  prevArrow:
    '<i class="slide-arrow prev-arrow fa fa-chevron-left" aria-hidden="true"></i>',
  nextArrow:
    '<i class="slide-arrow next-arrow fa fa-chevron-right" aria-hidden="true"></i>',
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,

        slidesToShow: 1,
      },
    },
  ],
});
$(".nav-slider").slick({
  // centerMode: true,
  dots: true,
  arrows: true,
  // prevArrow:
  //   '<i class="slide-arrow prev-arrow fa fa-chevron-left" aria-hidden="true"></i>',
  // nextArrow:
  //   '<i class="slide-arrow next-arrow fa fa-chevron-right" aria-hidden="true"></i>',
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 980,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,

        slidesToShow: 1,
      },
    },
  ],
});

$(document).ready(function () {
  $(".tab-a").click(function () {
    $(".tab").removeClass("tab-active");
    $(".tab[data-id='" + $(this).attr("data-id") + "']").addClass("tab-active");
    $(".tab-a").removeClass("active-a");
    $(this).parent().find(".tab-a").addClass("active-a");
  });
});

const pillsActive = () => {
  var pills = document.querySelectorAll("");
};
document
  .querySelectorAll('.dropdown-menu .dropdown-toggle[href="#"]')
  .forEach(function (element) {
    element.addEventListener("click", function (event) {
      event.stopPropagation();
    });
  });
